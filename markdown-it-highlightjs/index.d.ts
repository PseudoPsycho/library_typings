declare module 'markdown-it-highlightjs' {
    function hljs(md:any, options?:hljs.hljsOptions): void;

    namespace hljs {
        export interface hljsOptions {
        	auto?: boolean;
        	code?: boolean;
        }
    }

    export = hljs;
}